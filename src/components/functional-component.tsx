import * as React from 'react';

interface FunctionalComponentProps {
    name: string;
}

export const FunctionalComponent:React.SFC<FunctionalComponentProps> = (props) => {
    return (
        <div>
            <h1>Hello {props.name}</h1>
        </div>
    );
}
