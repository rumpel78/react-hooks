import * as React from 'react';
import { Router, Route, useRouter } from "react-router";

interface HooksComponentProps {
    name: string;
}

export const App:React.FunctionComponent = () => {
    <Router>
        <Route path="/" component={HooksComponent} />
    </Router>
}

const HooksComponent:React.FunctionComponent<HooksComponentProps> = (props) => {

    const [count, setCount] = React.useState(0);
    const route = useRouter();
   
    React.useEffect(() => {
        document.title = `You clicked ${count} times`;
    });

    return (
        <div>
            <h1>Current browser url: {route.match.url}</h1>
            <h1>Hello {props.name}</h1>
            <h1>Counter: {count}</h1>
            <button onClick={() => setCount( count + 1 )}>+</button>
            <button onClick={() => setCount( count - 1 )}>-</button>
        </div>
    );
}