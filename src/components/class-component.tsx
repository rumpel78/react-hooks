import * as React from 'react';

interface ClassComponentProps {
    name: string;
}

interface ClassComponentState {
    count: number;
}

export class ClassComponent extends React.Component<ClassComponentProps, ClassComponentState> {

    public state: ClassComponentState = {
        count: 0
    }

    componentDidMount() {
        document.title = `You clicked ${this.state.count} times`;
    }
    
    componentDidUpdate() {
        document.title = `You clicked ${this.state.count} times`;
    }

    public render() {
        return (
            <div>
                <h1>Hello {this.props.name}</h1>
                <h1>Counter: {this.state.count}</h1>
                <button onClick={() => this.setState(state => ({ count: state.count + 1 }))}>+</button>
                <button onClick={() => this.setState(state => ({ count: state.count - 1 }))}>-</button>
            </div>
        );
    }
}
