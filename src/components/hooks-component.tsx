import * as React from 'react';

interface HooksComponentProps {
    name: string;
}

export const HooksComponent:React.FunctionComponent<HooksComponentProps> = (props) => {

    const [count, setCount] = React.useState(0);
   
    React.useEffect(() => {
        document.title = `You clicked ${count} times`;
    });

    return (
        <div>
            <h1>Hello {props.name}</h1>
            <h1>Counter: {count}</h1>
            <button onClick={() => setCount( count + 1 )}>+</button>
            <button onClick={() => setCount( count - 1 )}>-</button>
        </div>
    );
}