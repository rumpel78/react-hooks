import React, { Component } from 'react';
import './App.css';
import { ClassComponent } from './components/class-component';
import { FunctionalComponent } from './components/functional-component';
import { HooksComponent } from './components/hooks-component';

class App extends Component {
  render() {
    return (
        <div>
          <HooksComponent name="fusonic" />
        </div>
    );
  }
}

export default App;
